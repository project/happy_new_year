<?php

/**
 * @file
 * Main file for the happy_new_year module.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function happy_new_year_help($route_name, RouteMatchInterface $route_match) {
  return '';
}

/**
 * Helper function.
 */
function _happy_new_year_isholidaytime() {
  $config = \Drupal::config('happy_new_year.settings');
  $start = $config->get('happy_new_year_start') + 1;
  $end = $config->get('happy_new_year_end') + 1;

  if (date('m') == 12 && date('d') >= $start) {
    return TRUE;
  }
  elseif (date('m') == 1 && date('d') <= $end) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Implements hook_page_attachments().
 */
function happy_new_year_page_attachments(array &$attachments) {
  $route = \Drupal::routeMatch()->getRouteObject();
  $is_admin = \Drupal::service('router.admin_context')->isAdminRoute($route);

  if (!$is_admin) {
    $config = \Drupal::config('happy_new_year.settings');

    if (($config->get('happy_new_year_period')) && !_happy_new_year_isholidaytime()) {
      return;
    }

    $garland = $config->get('happy_new_year_garland');
    $snow = $config->get('happy_new_year_snow');
    $minified = $config->get('happy_new_year_minified');
    $cdn = $config->get('happy_new_year_cdn');

    if ($garland) {
      $attachments['#attached']['library'][] = 'happy_new_year/garland';
      $attachments['#attached']['drupalSettings']['fixed_garland'] = $config->get('happy_new_year_garland_topfixed');
      $attachments['#attached']['drupalSettings']['garlandCoreToolbar'] = $config->get('happy_new_year_garland_coretoolbar');
      $attachments['#attached']['drupalSettings']['garlandBootstrapFixed'] = $config->get('happy_new_year_garland_bootstrapfixed');
      $attachments['#attached']['drupalSettings']['garlandCustomMargin'] = $config->get('happy_new_year_garland_custommargin');
      $attachments['#attached']['drupalSettings']['garlandCustomMarginText'] = $config->get('happy_new_year_garland_custommargintext');
    }

    if ($snow) {
      if ($minified) {
        if ($cdn) {
          $snowLibrary = 'happy_new_year/snowstorm-min-cdn';
        }
        else {
          $snowLibrary = 'happy_new_year/snowstorm-min';
        }
      }
      else {
        if ($cdn) {
          $snowLibrary = 'happy_new_year/snowstorm-cdn';
        }
        else {
          $snowLibrary = 'happy_new_year/snowstorm';
        }
      }

      $attachments['#attached']['library'][] = 'happy_new_year/snow';
      $attachments['#attached']['library'][] = $snowLibrary;
      $attachments['#attached']['drupalSettings']['happy_new_year']['snowcolor'] = $config->get('happy_new_year_snowcolor');
    }
  }
}
