<?php

namespace Drupal\happy_new_year\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class HnySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'happy_new_year_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'happy_new_year.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('happy_new_year.settings');

    $days = [];
    for ($i = 1; $i <= 31; $i++) {
      $days[] = $i;
    }

    $form = [];
    $form['period'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Working period'),
      '#attached' => [
        'library' => [
          'happy_new_year/settings-form',
        ],
      ],
    ];
    $form['period']['happy_new_year_period'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable working period'),
      '#default_value' => $config->get('happy_new_year_period'),
      '#description' => $this->t('If the working period is not specified, the module will run all the time'),
    ];
    $form['period']['period_dates'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Select start date and end date'),
      '#states' => [
        'visible' => [
          '#edit-happy-new-year-period' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['period']['period_dates']['happy_new_year_start'] = [
      '#type' => 'select',
      '#title' => $this->t('Start day (December)'),
      '#options' => $days,
      '#default_value' => $config->get('happy_new_year_start'),
    ];
    $form['period']['period_dates']['happy_new_year_end'] = [
      '#type' => 'select',
      '#title' => $this->t('End day (January)'),
      '#options' => $days,
      '#default_value' => $config->get('happy_new_year_end'),
    ];
    $form['snow_and_garland'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Garland and Snow'),
    ];
    $form['snow_and_garland']['happy_new_year_garland'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable garland'),
      '#default_value' => $config->get('happy_new_year_garland'),
    ];
    $form['snow_and_garland']['garland_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Garland settings'),
      '#states' => [
        'visible' => [
          '#edit-happy-new-year-garland' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['snow_and_garland']['garland_settings']['happy_new_year_garland_topfixed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Top-Fixed garland'),
      '#default_value' => $config->get('happy_new_year_garland_topfixed'),
    ];
    $form['snow_and_garland']['garland_settings']['happy_new_year_garland_coretoolbar'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Drupal core toolbar is used (add garland after toolbar)'),
      '#default_value' => $config->get('happy_new_year_garland_coretoolbar'),
    ];
    $form['snow_and_garland']['garland_settings']['happy_new_year_garland_bootstrapfixed'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Bootstrap navbar-fixed is used (add garland after navbar)'),
      '#default_value' => $config->get('happy_new_year_garland_bootstrapfixed'),
    ];
    $form['snow_and_garland']['garland_settings']['happy_new_year_garland_custommargin'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use custom top margin'),
      '#default_value' => $config->get('happy_new_year_garland_custommargin'),
    ];
    $form['snow_and_garland']['garland_settings']['happy_new_year_garland_custommargintext'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Enter custom top margin'),
      '#default_value' => $config->get('happy_new_year_garland_custommargintext'),
      '#maxlength' => 7,
      '#size' => 7,
      '#states' => [
        'visible' => [
          '#edit-happy-new-year-garland-custommargin' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['snow_and_garland']['happy_new_year_snow'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable snow'),
      '#default_value' => $config->get('happy_new_year_snow'),
    ];
    $form['snow_and_garland']['snow_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Snow settings'),
      '#states' => [
        'visible' => [
          '#edit-happy-new-year-snow' => ['checked' => TRUE],
        ],
      ],
    ];
    $form['snow_and_garland']['snow_settings']['happy_new_year_snowcolor'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Snow color'),
      '#suffix' => '<div id="color-picker"></div>',
      '#default_value' => $config->get('happy_new_year_snowcolor') ?: '#FFFFFF',
      '#maxlength' => 7,
      '#size' => 7,
      '#attached' => [
        'library' => [
          'core/jquery.farbtastic',
          'happy_new_year/colorpicker',
        ],
      ],
    ];
    $form['happy_new_year_minified'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use minified libraries (recommended for production)'),
      '#default_value' => $config->get('happy_new_year_minified'),
    ];
    $form['happy_new_year_cdn'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Load libraries from CDN (github)'),
      '#description' => $this->t('If you want to load libraries from local directory, see happy_new_year.libraries.yml to actual routes'),
      '#default_value' => $config->get('happy_new_year_cdn'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('happy_new_year.settings')
      ->set('happy_new_year_period', $values['happy_new_year_period'])
      ->set('happy_new_year_start', $values['happy_new_year_start'])
      ->set('happy_new_year_end', $values['happy_new_year_end'])
      ->set('happy_new_year_garland', $values['happy_new_year_garland'])
      ->set('happy_new_year_garland_topfixed', $values['happy_new_year_garland_topfixed'])
      ->set('happy_new_year_garland_coretoolbar', $values['happy_new_year_garland_coretoolbar'])
      ->set('happy_new_year_garland_bootstrapfixed', $values['happy_new_year_garland_bootstrapfixed'])
      ->set('happy_new_year_garland_custommargin', $values['happy_new_year_garland_custommargin'])
      ->set('happy_new_year_garland_custommargintext', $values['happy_new_year_garland_custommargintext'])
      ->set('happy_new_year_snow', $values['happy_new_year_snow'])
      ->set('happy_new_year_snowcolor', $values['happy_new_year_snowcolor'])
      ->set('happy_new_year_minified', $values['happy_new_year_minified'])
      ->set('happy_new_year_cdn', $values['happy_new_year_cdn'])
      ->save();

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

}
