(function ($, Drupal) {
  Drupal.behaviors.snow = {
    attach() {
      // eslint-disable-next-line
      snowStorm.snowColor = drupalSettings.happy_new_year.snowcolor;
      // eslint-disable-next-line
      snowStorm.className = 'snowflake';
    },
  };
})(jQuery, Drupal);
