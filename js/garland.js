(function ($, Drupal) {
  Drupal.behaviors.garland = {
    attach(context, settings) {
      let num = 0;
      function garland() {
        // eslint-disable-next-line
        $('#garland').css('backgroundPosition', `0 -${num}px`);
        if (num > 72) {
          num = 36;
        } else {
          num += 36;
        }
      }

      // if garland not exists.
      if ($('#garland').length === 0) {
        $('body').prepend('<div id="garland"></div>');
      }

      if ($('#garland').length > 0) {
        if (drupalSettings.fixed_garland) {
          // eslint-disable-next-line
          $('#garland').css('position', 'fixed');
        } else {
          // eslint-disable-next-line
          $('#garland').css('position', 'absolute');
        }

        // if core toolbar or admin_toolbar exists.
        if (drupalSettings.garlandCoreToolbar) {
          if ($('#toolbar-bar').length > 0) {
            const toolbarHeight =
              $('#toolbar-bar').height() +
              $('#toolbar-item-administration-tray').height();
            // eslint-disable-next-line
            $('#garland').css('top', `${toolbarHeight}px`);
            // eslint-disable-next-line
            $('#garland').css('zIndex', '1');
          }
        }

        // if bootstrap navbar fixed top exists.
        if (drupalSettings.garlandBootstrapFixed) {
          if ($('.navbar-fixed-top').length > 0) {
            const navbarHeight = $('.navbar-fixed-top').height();
            const navbarTop = $('.navbar-fixed-top').position().top;
            // eslint-disable-next-line
            $('#garland').css('top', `${navbarTop + navbarHeight}px`);
          }
        }

        // Custom margin.
        if (drupalSettings.garlandCustomMargin) {
          const top = drupalSettings.garlandCustomMarginText;
          if (top > 0) {
            // eslint-disable-next-line
            $('#garland').css('top', `${top}px`);
          }
        }
      }

      setInterval(function () {
        garland();
      }, 500);
    },
  };
})(jQuery, Drupal);
