(function ($, Drupal) {
  Drupal.behaviors.colorPicker = {
    attach(context, settings) {
      // eslint-disable-next-line
      $(document).ready(function () {
        $('#color-picker').farbtastic('#edit-happy-new-year-snowcolor');
      });
    },
  };
})(jQuery, Drupal);
